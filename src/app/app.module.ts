import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap' ;
import { AppComponent } from './app.component';
import { ItemsComponent } from './items/items.component';
import { ItemComponent } from './items/item/item.component';
import { ItemListComponent } from './items/item-list/item-list.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { ItemStatisticComponent } from './items/item-statistic/item-statistic.component';
import { ItemService } from './items/shared/item.service';

@NgModule({
  declarations: [
    AppComponent,
    ItemsComponent,
    ItemComponent,
    ItemListComponent,
    ItemStatisticComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot(),
    NgxPaginationModule
  ],
  providers: [ItemService],
  bootstrap: [AppComponent]
})
export class AppModule { }
