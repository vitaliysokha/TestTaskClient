import { Component, OnInit } from '@angular/core';
import{ItemService} from '../shared/item.service'
import { Item } from '../../../app/items/shared/item.model';
@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {

  constructor(private itemService: ItemService) { }

  ngOnInit() {
    this.itemService.getItemList();
  }
  showForEdit(emp: Item) {
    this.itemService.selectedItem = Object.assign({}, emp);;
  }
 
 
  onDelete(id: number) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.itemService.deleteItem(id)
      .subscribe(x => {
        this.itemService.getItemList();
       // this.toastr.warning("Deleted Successfully","Employee Register");
      })
    }
  }

}
