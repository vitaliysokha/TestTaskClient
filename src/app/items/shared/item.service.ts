import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import {Item} from './item.model'
import {Statistic} from './item.model';

@Injectable()
export class ItemService {

  selectedItem : Item;
  itemList : Item[];
  itemStatistic: Statistic[];
  private url = 'http://localhost:63143/api/Items/';
  constructor(private http: Http) { }

  postItem(name, type){
    const item: Item = {
      Name: name,
      Type: type
    }
    var body = JSON.stringify(item);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post(this.url,body,requestOptions).map(x => x.json());
  }
  putItem(id, item) {
    var body = JSON.stringify(item);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Put, headers: headerOptions });
    return this.http.put(this.url + id,
      body,
      requestOptions).map(res => res.json());
  }
 
  getItemList(){
    this.http.get(this.url)
    .map((data : Response) =>{
      return data.json() as Item[];
    }).toPromise().then(x => {
      this.itemList = x;
    })
  }

  getItemStatistic(){
    this.http.get(`${this.url}GetStatistic`)
    .map((data : Response) =>{
      return data.json() as Statistic[];
    }).toPromise().then(x => {
      this.itemStatistic = x;
      console.log(this.itemStatistic);
    })
  }
 
  deleteItem(id: number) {
    return this.http.delete(this.url + id).map(res => res.json());
  }
}
