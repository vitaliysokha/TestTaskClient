import { Component, OnInit } from '@angular/core';
import { ItemService } from '../shared/item.service';

@Component({
  selector: 'app-item-statistic',
  templateUrl: './item-statistic.component.html',
  styleUrls: ['./item-statistic.component.css']
})
export class ItemStatisticComponent implements OnInit {

  constructor(private itemService: ItemService) { }

  ngOnInit() {
    this.itemService.getItemStatistic();
  }

}
