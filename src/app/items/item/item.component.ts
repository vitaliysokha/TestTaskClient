import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'
import{ItemService} from '../shared/item.service'
import { Item } from '../shared/item.model';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  item: Item;

  constructor(private itemService: ItemService) { }

  ngOnInit() {
    this.resetForm();
  }
  
  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.itemService.selectedItem = {
      Id: null,
      Name: '',
      Type: ''
    }
  }

  onSubmit(form: NgForm) {
    if (form.value.Id == null) {
      this.itemService.postItem(form.value.Name, form.value.Type)
        .subscribe(data => {
          this.resetForm(form);
          this.itemService.getItemList();
        //  this.toastr.success('New Record Added Succcessfully', 'Employee Register');
        })
    }
    else {
      this.itemService.putItem(form.value.Id, form.value)
      .subscribe(data => {
        this.resetForm(form);
        this.itemService.getItemList();
       // this.toastr.info('Record Updated Successfully!', 'Employee Register');
      });
    }
  }
}
